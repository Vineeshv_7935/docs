# developer 容器应用开发指导

## 环境准备

### edgegallery 安装v1.2版本 
#### developer 安装注意事项-镜像仓库配置<br/>
1.2版本部署yaml上传的时候，新增image校验，校验规则如下：<br/>

 **1.** image配置为edgegallery镜像仓库地址（包括环境变量的配置形式），首先会去edgegallery仓库进行拉取，如果拉取失败，提示上传者镜像信息配置有误（镜像地址配置错误或者未提前上传镜像到edgegallery仓库）。<br/>

 **2.** image配置为其他镜像仓库地址（例如swr,docker等仓库），首先会进行拉取，如果拉取失败，提示上传者镜像信息配置有误（镜像地址配置错误，开源仓库中未找到此镜像）。拉取成功，developer后台会对此镜像进行重新打标签，推到edgegallery仓库，下次开发者可以直接配置成edgegallery仓库的镜像地址形式，进行容器应用的开发。<br/>

 **3.** 镜像信息配置为其他形式（非edgegallery镜像仓库镜像地址），因为镜像大小以及仓库的不同，以及后续的重新打标签，推送操作，可能会有稍微的费时，请开发者上传yaml时耐心等待。
#### developer 安装注意事项-Docker配置<br/>

 **1.** 因为涉及到开发者连接edgegallery安装环境的docker去其他开源镜像仓库拉取镜像，因此开放2375端口（其他未被占用端口也可以）。以下操作以环境为Ubuntu18.04,Docker18.09.0作为样例说明，不同系统，docker的安装目录也不尽相同，具体原理大体一致。<br/>
- 查找docker.service的目录，操作命令systemctl status docker
![输入图片说明](https://images.gitee.com/uploads/images/2021/0630/170158_44bd012f_5504908.png "dockerservice-new.png")
- 编辑docker.service,添加如下内容<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0630/170510_ee349bc5_5504908.png "dockerservicecontent.png")
- systemctl daemon-reload //重新加载系统的daemon,使配置生效
- service docker restart  //重启docker<br/>

 **2.** 接上面的步骤，重启docker之后，查看/var/run/docker.sock的权限，如果docker.sock的权限中的o权限为0，手动为其添加rw权限
最终权限如下：<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0630/171606_83e1797d_5504908.png "dockersock.png")
<br/>
## 配置沙箱环境：developer新增K8S沙箱环境

 **1.** 利用admin账户登录Developer平台，打开系统-沙箱管理，点击左上角新增沙箱环境按钮，添加k8的沙箱<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0701/143746_1e95d967_5504908.png "k8s.png")<br/>
**2.** 参数说明：<br/>
 **名称** 边缘节点（沙箱环境）的名称&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **lcmIp** lcmcontroller的ip地址<br/>
 **mecHost:** 边缘节点的IP地址&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **端口号** lcmcontroller对外暴露的端口号<br/>
 **协议** 选择https&nbsp;&nbsp; **用户名** 登录mechost的用户名&nbsp;&nbsp; **密码** 登录mechost的密码<br/>
 **架构** 根据自己的需要选择&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **部署区域** 根据自己的需要填写&nbsp;&nbsp;&nbsp;**其他** 可以不用改动<br/>
**3.** 上传的配置文件结构如下，自己配置部分已由xxx代替：<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0701/151313_489a9540_5504908.png "config.png")

## 容器应用开发
### 新建容器应用项目或者快速集成项目，主要的是选择容器属性，因为涉及到后续的发布能力，以新建项目为例作出说明
#### 1. 进入开发者平台，选择导航栏中的工作空间，点击项目列表右上方的新建项目，选择应用开发
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/113102_e12792fe_5504908.png "新建项目.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/113150_d7c948dd_5504908.png "集成或者新建.png")<br/>
#### 2. 填写项目基本信息，切记勾选负载类型为容器，新建第二部选择服务发现能力
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/113407_e13c5fd3_5504908.png "新建1.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/113427_9c4b2d9a_5504908.png "新建2.png")<br/>
#### 3. 进入项目详情页，查看项目属性
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/113527_ea4193f9_5504908.png "详情.png")<br/>
#### 4. 进入能力详情页，展示的为勾选的能力的api
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/113734_b718d554_5504908.png "能力详情.png")<br/>
#### 5. 进入应用开发页，最主要的可以下载样例代码，进行本地开发
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/113943_7be5a4b2_5504908.png "应用开发.png")<br/>
#### 6. 新建项目完成，进入项目详情页，可直接点击左边的部署调测到达此页，可以忽略能力详情和应用开发，跳转到此步进行测试<br/>
#### （1）上传镜像，如果您需要测试的镜像可以从公共的镜像仓库拉取，可以直接进入第二步配置部署文件。反之，请先上传tar格式的镜像至EdgeGallery镜像仓库，如果您已通过其他方式将镜像推送至EdgeGallery镜像仓库，可直接进入第二步配置部署文件。<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/142510_f3377eab_5504908.png "部署调测1.png")<br>
#### （2）配置部署文件，可以选择上传yaml或者进行可视化配置，如果选择上传，可直接点击模板下载链接下载样例文件，进行修改并且上传，选择可视化配置，正常填写就行。<br/>
**上传**<br/>
![](https://images.gitee.com/uploads/images/2021/0705/143136_afd8a588_5504908.png "部署调测2.png")<br/>
**可视化配置**<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/144525_ccc745a7_5504908.png "可视化.png")<br/>
#### （3）部署调测。选择测试环境，尽量选择沙箱环境，真实的5G环境较少，可能会申请失败。点击开始部署，直至出现部署成功，可以进行远程VNC连接，进入沙箱环境，查看已经部署的项目，也可以测试出现的url，是否可用。部署失败也会显示失败原因。最底下会显示应用的pod构成，资源占比。<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/145123_86b9ef5f_5504908.png "部署3.png")<br/>
#### 7. 应用发布，进入发布页之前，记得释放资源。可以在部署调测页点击释放资源按钮释放，如果未点击释放资源进入发布页，发布页也会弹窗提示释放资源<br/>
#### （1）应用配置，此页面可以根据自己需要配置，不做强制要求。可以上传项目的说明文档，也可以配置dns，流量规则，也可以添加您需要对外发布的服务。<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/145937_fc649708_5504908.png "应用发布1.png")
#### （2）应用认证，是对您经过开发者平台部署调测，配置生成的应用包进行解析，测试，包括文件目录校验、病毒扫描等。<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/150838_a7c1486c_5504908.png "应用发布2.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/150914_9f97acef_5504908.png "应用发布2-2.png")<br/>
#### （3）应用发布，认证成功的应用可以点击发布按钮，将其发布到Appstore（应用市场）。如果应用配置中，配置了服务，也可以选择是否把服务发布到开发者的能力中心，发布成功，可以在应用市场的我的应用中查看已发布的应用<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/151314_53d74b22_5504908.png "应用发布3.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/151335_9683c789_5504908.png "应用发布3-2.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0705/151356_1a02a11a_5504908.png "appstore.png")<br/> 
## 常见问题
#### 1.开发者对容器应用部署调测时，可以下载开发者平台提供的样例yaml，根据自己需要部署的应用，修改样例yaml并上传使用。<br/> 
#### 2.开发者平台会对上传的yaml中的镜像信息进行校验，yaml中配置的镜像不拘泥于固定格式，但是需要注意的是镜像需要为开源镜像或者EdgeGallery仓库已有镜像。<br/> 
#### 3.如果yaml中配置的镜像形式为EdgeGallery仓库镜像形式，请提前上传镜像到EdgeGallery仓库，防止容器应用部署调测失败。<br/> 
#### 4.如果yaml中配置的镜像形式为EdgeGallery仓库镜像形式，建议的镜像配置格式如下：<br/> 
#### ‘{{.Values.imagelocation.domainname}}/{{.Values.imagelocation.project}}/xxx:xxx’<br/> 
#### 5.如果yaml中配置的镜像属于其他开源镜像仓库，请确保镜像配置形式正确。<br/> 
#### 6.请尽可能的配置yaml中的namespace，并确保其格式为’{{ .Values.appconfig.appnamespace }}’。<br/> 
#### 7.尽可能一次性配置正确，确保yaml中的关键参数配置正确，例如暴露的端口，配置的环境变量等等。<br/> 
## 容器应用与MepAgent的集成
### 参考文档：[APP通过MEP调用](https://gitee.com/edgegallery/docs/blob/master/Projects/MEP/app%E9%80%9A%E8%BF%87MEP%E8%B0%83%E7%94%A8.md)